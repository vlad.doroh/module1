﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module = new Module1();

            int[] array = module.SwapItems(3, 0);

            Console.Write("Array elements: ");
            foreach (var item in array)
            {
                Console.Write(item + "\t");
            }

            Console.WriteLine("\nMinimum value: " + module.GetMinimumValue(array));
        }

        public int[] SwapItems(int a, int b)
        {
            return new int[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int minimumValue = input[0];

            foreach (var item in input)
            {
                if (minimumValue > item)
                    minimumValue = item;
            }

            return minimumValue;
        }
    }
}
